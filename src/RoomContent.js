import React, { useState } from 'react';
import $ from 'jquery';
import { CSSTransition } from 'react-transition-group';
import { Modal } from 'react-bootstrap';
var qiscus
var typingTimer = -1
var isTyping = 0

export default class RoomContent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            onLoadMore: false,
            tempDeleteid: ''
        }

        qiscus = this.props.qiscus
    }

    componentDidMount() {
        $('.messages').scroll(() => {
            if ($('.options.active').length > 0) $('.options.active').removeClass('active')
        })
    }

    onSendMsg = (e) => this.props.onSendMsg(true)
    handleLoadMore = () => this.props.loadMore()
    handleOpen = (id) => {
        this.setState({tempDeleteid: id})
        this.props.toggleDeleteMsgModal(true)
    }
    handleClose = () => this.props.toggleDeleteMsgModal(false)
    handleExit = () => { if (this.props.onDeleteMsg) this.props.toggleDeleteMsgModal(true) }
    handleDeleteMsg = () => this.props.deleteMsg(this.state.tempDeleteid)
    handleDeleteRoom = () => this.props.deleteRoom()

    render() {
        let messagesClass = 'messages'
        if (this.props.isLoad) messagesClass += ' item-flex-middle'
        return (
            <div className="home">
                <HomeHeader {...this.props} deleteRoom={this.handleDeleteRoom}/>
                <div className={messagesClass}>
                    {this.props.hasLoadMore &&
                        <div className="load-more"><button onClick={() => this.handleLoadMore()}>{this.props.onLoadMore ? <div className="loader sm"></div> : 'Load more'}</button></div>
                    }
                    <Messages {...this.props} openModal={this.handleOpen} />
                </div>
                {this.props.room !== '' && <HomeFooter {...this.props} onSendMsg={this.onSendMsg}/>}
                <Modal size="sm" centered show={this.props.deleteMsgModal} onHide={this.handleClose} onExit={this.handleExit}>
                    <Modal.Header closeButton>
                    <Modal.Title id="room-modal-title">Delete Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.onDeleteMsg ? (
                            <div className="item-flex-middle"><div className="loader md"></div></div>
                        ) : (
                            <label className="m-0 text-center">Deleting message will only take effect on your account.</label>
                        )}
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn-sm btn-secondary" disabled={this.props.onDeleteMsg} onClick={this.handleClose}>Cancel</button>
                        <button className="btn-sm btn-danger" disabled={this.props.onDeleteMsg} onClick={this.handleDeleteMsg}>Proceed</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

class HomeHeader extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menu: false,
            deleteRoomModal: false,
            onRoomDelete: false
        }
    }

    componentDidMount() {
        $(window).click((e) => {
            if (!$(e.target).hasClass('room-menu')) this.setState({ menu: false })
        })
    }

    toggleSidebar = () => this.props.toggleSidebar(true)
    handleClose = () => this.setState({ deleteRoomModal: false })
    handleExit = () => {
        if (this.state.onRoomDelete) this.setState({ deleteRoomModal: true })
    }
    handleDeleteRoom = () => {
        this.setState({ onRoomDelete: true })
        qiscus.clearRoomMessages([this.props.room.unique_id]).then((rooms) => {
            this.setState({ onRoomDelete: false })
            this.handleClose()
            this.props.deleteRoom()
        }).catch((err) => alert(err))
    }

    render() {
        let typing = ''
        let avatar = 'avatar'
        if (this.props.isTyping) typing = 'typing'
        if (this.props.avatar === '') avatar += ' hide'

        return (
            <div className="header">
                <svg version="1.1" className="open-sidebar" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" style={{enableBackground:'new 0 0 512 512'}} onClick={this.toggleSidebar}>
                    <path d="M491.318,235.318H20.682C9.26,235.318,0,244.577,0,256s9.26,20.682,20.682,20.682h470.636c11.423,0,20.682-9.259,20.682-20.682C512,244.578,502.741,235.318,491.318,235.318z"/><path d="M491.318,78.439H20.682C9.26,78.439,0,87.699,0,99.121c0,11.422,9.26,20.682,20.682,20.682h470.636 c11.423,0,20.682-9.26,20.682-20.682C512,87.699,502.741,78.439,491.318,78.439z"/><path d="M491.318,392.197H20.682C9.26,392.197,0,401.456,0,412.879s9.26,20.682,20.682,20.682h470.636 c11.423,0,20.682-9.259,20.682-20.682S502.741,392.197,491.318,392.197z"/>
                </svg>
                {this.props.room !== "" &&
                <>
                    <img src={this.props.avatar} alt="Target avatar" className={avatar} />
                    <h3 className="name mb-0 ml-2 font-weight-bold">
                        {this.props.name}
                        <span className={typing}>{this.props.status}</span>
                    </h3>
                    <div className="menu">
                        <svg version="1.1" className="room-menu" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 451.847 451.847" style={{enableBackground:'new 0 0 451.847 451.847'}} onClick={() => this.setState({ menu: !this.state.menu })}>
                            <path className="room-menu" d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27, 151.157c-12.359-12.359-12.359-32.397,0-44.751 c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/>
                        </svg>
                        <CSSTransition
                            in={this.state.menu}
                            timeout={100}
                            classNames="fade-menu"
                            unmountOnExit
                        >
                            <div>
                            <button onClick={() => this.setState({ deleteRoomModal: true })}>Delete all message</button>
                            </div>
                        </CSSTransition>
                    </div>
                </>
                }
                <Modal size="sm" show={this.state.deleteRoomModal} centered onHide={this.handleClose} onExit={this.handleExit}>
                    <Modal.Body>
                        <h5 className="text-center">Are you sure want to delete your conversation with <span className="font-weight-bold">{this.props.name}</span> ?</h5>
                        <div className="row">
                            <div className="col-6 pr-2">
                                <button className="btn btn-outline-secondary btn-sm btn-block" onClick={this.handleClose}>Cancel</button>
                            </div>
                            <div className="col-6 pl-2">
                                <button className="btn btn-outline-danger btn-sm btn-block" onClick={this.handleDeleteRoom}>Delete</button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

class Messages extends React.Component {
    constructor(props) {
        super(props)
        this.handleBubbleClick = this.handleBubbleClick.bind(this)
    }

    componentDidMount() {
        $(window).click(() => {
            if ($('.options.active').length > 0) $('.options.active').removeClass('active')
        })
    }

    handleBubbleClick = (event) => {
        let el = $(event.target)
        let isActive = el.parents('.options.active').length > 0 ? true : false
        $('.options').removeClass('active top')
        $('.options .menu').removeClass('right')
        setTimeout(() => {
            if (!isActive) el.parents('.options').addClass('active')

            let chatContent = $('.messages').innerHeight()
            let menu = $('.options.active .menu').innerHeight()
            if (el.parents('.bubble').offset().top > (chatContent - menu)) {
                el.parents('.options').addClass('top')
                el.parents('.options').find('.menu').css({top:'-'+menu+'px'})
            } else { el.parents('.options').find('.menu').css({top:'2rem'}) }
        },1)
        if (el.parents('.bubble').width() < 160) el.parents('.options').find('.menu').addClass('right')
    }

    handleDelete = (id) => {
        this.props.openModal(id)
    }

    render() {
        if (this.props.isLoad) {
            return (<div className="loader lg"></div>)
        } else {
            if (this.props.room === '') {
                return (<h5>Please create a room or select an existing one to start chatting.</h5>)
            } else {
                let msg = this.props.messages
                if (msg.length === 0) {
                    return (<label className="text-muted">There is nothing to see here.</label>)
                } else {
                    let messages = []
                    for (let i = 0; i < msg.length; i++) {
                        let isMe = msg[i].pos === 'right' ? true : false
                        let msgClass = "message " + msg[i].pos
                        let bubbleClass = "bubble"
                        if (i !== 0) {
                            if (msg[i-1].pos !== msg[i].pos || msg[i-1].date.date !== msg[i].date.date) bubbleClass += ' first'
                        } else {
                            bubbleClass += ' first'
                        }
                        if (i < msg.length - 1) {
                            if (msg[i+1].pos !== msg[i].pos || msg[i+1].date.date !== msg[i].date.date) {
                                msgClass += ' mb-3'
                                bubbleClass += ' last'
                            }
                        }
                        if (i === msg.length - 1) bubbleClass += ' last'
                        if (msg[i].payload.content) bubbleClass += ' attachment'
                        messages.push(
                            <div className={msgClass} key={msg[i].id}>
                                {i !== 0 ? (
                                    msg[i-1].date.date !== msg[i].date.date && <div className="date">{msg[i].date.date}</div>
                                    ) : (<div className="date">{msg[i].date.date}</div>)
                                }
                                <div className="chat">
                                    {isMe &&
                                        <img className="status" src={"./" + msg[i].status + ".svg"} alt="Message status" title={msg[i].status.charAt(0).toUpperCase() + msg[i].status.slice(1)} />
                                    }
                                    <div className={bubbleClass}>
                                        <div className="options" onClick={this.handleBubbleClick}>
                                            <span className="icon">
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 451.847 451.847" style={{enableBackground:'new 0 0 451.847 451.847'}}>
                                                    <path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27, 151.157c-12.359-12.359-12.359-32.397,0-44.751 c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/>
                                                </svg>
                                            </span>
                                            <span className="menu">
                                                <button onClick={() => this.handleDelete(msg[i].unique_id)} disabled={!isMe}>Delete message</button>
                                            </span>
                                        </div>
                                        {msg[i].payload.content && <ChatAttachment payload={msg[i].payload} />}
                                        {msg[i].msg}
                                    </div>
                                    <div className="info">{msg[i].date.time}</div>
                                </div>
                            </div>
                        )
                    }

                    return(messages)
                }
            }
        }
    }
}

function ChatAttachment(props) {
    let attachmentClass = "attachment"
    let isFile = props.payload.content.url.match(/\.(jpeg|jpg|gif|png)$/) == null ? true : false
    if (isFile) attachmentClass += ' file'
    return (
        <span className={attachmentClass}>
            {isFile ? 
                (
                    <>
                    <img src="./file.svg" width="40" alt="File icon" />
                    <label className="text-truncate" style={{maxWidth:'11rem'}} title={props.payload.content.file_name}>{props.payload.content.file_name}</label>
                    </>
                ) : (<img src={props.payload.content.url} alt="Attachment" style={{width:'100%',height:'auto', borderRadius:'0.5rem'}} />)
            }
        </span>
    )
}

class HomeFooter extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            file: '',
            attachment: '',
            filename: '',
            isFile: true,
            onUpload: false,
            uploadProgress: 0,
            uploadDone: false
        }
    }
    handleTyping = () => {
        if (!isTyping) {
            qiscus.publishTyping(1)
            isTyping = 1
        }

        window.clearTimeout(typingTimer)
        typingTimer = setTimeout(() => {
            qiscus.publishTyping(0)
            isTyping = 0
        }, 1000)
    }

    sendMsgByEnter = (e) => {
        if (!$('.btn-send-msg').is('disabled')) {
            if (e.keyCode === 13) {
                if (!e.shiftKey) {
                    e.preventDefault()
                    this.handleSendMsg()
                }
            }
        }
    }

    handleSendMsg = () => {
        if ($('.send-msg-input').val() === '') {
            $('.send-msg-input').focus()
            return false
        }

        this.props.onSendMsg(true)
        qiscus.sendComment(this.props.room.id, $('.send-msg-input').val()).catch((error) => alert(error))
    }

    handleFileInput = (file) => {
        if (file.name) {
            let isFile = file.type.substr(0,5) === "image" ? false : true
            let attachment = isFile ? (<div className="d-flex mb-2" style={{maxHeight: "5rem"}}>
                <img className="mx-auto" src="./file.svg" alt="File" />
                </div>) : (<div style={{width: '100%', maxHeight: '25rem'}}>
                    <div className="d-flex mb-2">
                        <div className="mx-auto">
                            <img id="image-upload-preview" src="./image.svg" alt="Preview" style={{maxWidth: '100%', maxHeight: '25rem'}} />
                        </div>
                    </div>
                </div>)
            if (!isFile) {
                let reader = new FileReader()
                reader.onload = (e) => { $('#image-upload-preview').attr('src', e.target.result) }
                reader.readAsDataURL(file)
            }
            this.setState({file: file, filename: file.name, isFile: isFile, attachment: attachment})
            setTimeout(() => {$('.send-file-caption').focus()},50)
        }
        $('#uploadFile').val('')
    }

    handleSendFile = () => {
        this.props.onSendFile()
        let caption = $('.send-file-caption').val()
        this.setState({
            onUpload: true,
            uploadProgress: 0,
            uploadDone: false
        })
        qiscus.upload(this.state.file, (error, progress, url) => {
            if (error) alert(error)
            if (progress) this.setState({uploadProgress: progress.percent})

            if (url) {
                this.setState({uploadDone: true})
                var roomId = this.props.room.id
                var uniqueId = Date.now()
                var type = 'custom'
                var payload = JSON.stringify({
                    type: 'file',
                    content: {
                        url: url,
                        caption: caption,
                        file_name: this.state.filename
                    }
                })
                var extras = ''

                qiscus.sendComment(roomId, caption, uniqueId, type, payload, extras).then((comment) => {
                    $('.send-file-caption').val('')
                    this.setState({onUpload: false})
                }).catch((error) => alert(error))
            }
        })
    }

    render() {
        return (
            <div className="footer">
                <div className="d-flex">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <SendFile
                                sendingMsg={this.props.sendingMsg}
                                sendingFile={this.props.sendingFile}
                                {...this.state}
                                onFileInput={this.handleFileInput}
                                onSendFile={this.handleSendFile}
                            />
                        </div>
                        <textarea className="form-control mx-2 send-msg-input" rows="1" placeholder="Type a message" onInput={() => this.handleTyping()} onKeyDown={(event) => this.sendMsgByEnter(event)} disabled={this.props.sendingMsg}></textarea>
                        <div className="input-group-append">
                            <button className="btn-send-msg btn-main" onClick={() => this.handleSendMsg()} disabled={this.props.sendingMsg} title="Send message">
                                {this.props.sendingMsg ?
                                    (<div className="loader sm"></div>) :
                                    (<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 477.808 477.808" style={{enableBackground: 'new 0 0 477.808 477.808', width: '1.75rem', fill: '#fff'}}><rect x="205.781" y="122.359" transform="matrix(0.9078 0.4194 -0.4194 0.9078 78.4686 -80.2537)" width="32" height="32"/><rect x="147.686" y="95.539" transform="matrix(0.908 0.419 -0.419 0.908 61.7892 -58.3169)" width="32" height="32"/><path d="M477.808,238.904l-205.728-94.96l-13.408,29.072l108.112,49.888H84.128l-36.8-147.472l66.064,30.528L126.8,76.904 L0,18.376l55.136,220.528L0,459.432L477.808,238.904z M84.208,254.904h282.576L47.264,402.376L84.208,254.904z"/></svg>)
                                }
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function SendFile(props) {
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)
    const handleExit = () => { if (props.onUpload) handleShow() }
    const fileCheck = () => {
        handleShow()
        props.onFileInput($('#uploadFile')[0].files[0])
    }
    const sendFile = () => props.onSendFile()

    let fileUploadClass = "btn-main rounded-left"
    if (props.sendingMsg) fileUploadClass += " disabled"
    if (props.sendingFile && show && !props.onUpload) handleClose()

    return (
        <>
            <input type="file" className="d-none" id="uploadFile" onChange={fileCheck} disabled={props.sendingMsg}/>
            <label className={fileUploadClass} title="Attach file" htmlFor="uploadFile">
                <svg viewBox="0 0 448 448" xmlns="http://www.w3.org/2000/svg" style={{width:'1rem',fill:'#fff'}}><path d="m408 184h-136c-4.417969 0-8-3.582031-8-8v-136c0-22.089844-17.910156-40-40-40s-40 17.910156-40 40v136c0 4.417969-3.582031 8-8 8h-136c-22.089844 0-40 17.910156-40 40s17.910156 40 40 40h136c4.417969 0 8 3.582031 8 8v136c0 22.089844 17.910156 40 40 40s40-17.910156 40-40v-136c0-4.417969 3.582031-8 8-8h136c22.089844 0 40-17.910156 40-40s-17.910156-40-40-40zm0 0"/></svg>
            </label>
            <Modal size="lg" show={show} centered onHide={handleClose} onExit={handleExit}>
                <Modal.Header closeButton>
                    <Modal.Title>Preview</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {props.onUpload && props.sendingFile ? (<>
                        <h5 className="text-center">Uploading file : {props.uploadDone ? 'Done' : 'On progress'}</h5>
                        <div className="progress">
                            <div className="progress-bar bg-main" role="progressbar" style={{width: props.uploadProgress + '%'}} aria-valuenow={props.uploadProgress} aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        {props.uploadDone && <h5 className="text-center mt-3">Sending file</h5>}
                        </>) : (<>
                        {props.attachment}
                        <div className="text-center"><label className="text-truncate w-75" title={props.filename}>{props.filename}</label></div>
                        <div className="input-group">
                            <input className="form-control send-file-caption" placeholder="Add caption" />
                            <div className="input-group-append">
                                <button className="btn-main btn-send-file" onClick={sendFile}>
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 477.808 477.808" style={{enableBackground: 'new 0 0 477.808 477.808', width: '1.75rem', fill: '#fff'}}><rect x="205.781" y="122.359" transform="matrix(0.9078 0.4194 -0.4194 0.9078 78.4686 -80.2537)" width="32" height="32"/><rect x="147.686" y="95.539" transform="matrix(0.908 0.419 -0.419 0.908 61.7892 -58.3169)" width="32" height="32"/><path d="M477.808,238.904l-205.728-94.96l-13.408,29.072l108.112,49.888H84.128l-36.8-147.472l66.064,30.528L126.8,76.904 L0,18.376l55.136,220.528L0,459.432L477.808,238.904z M84.208,254.904h282.576L47.264,402.376L84.208,254.904z"/></svg>
                                </button>
                            </div>
                        </div>
                        </>)
                    }
                </Modal.Body>
            </Modal>
        </>
    )
}