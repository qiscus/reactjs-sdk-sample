import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import * as serviceWorker from './serviceWorker';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './reboot.css';
import './index.css';
import HomePage from './HomePage';
import QiscusSDK from 'qiscus-sdk-core';
const qiscus = new QiscusSDK();

class Init extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            login: localStorage.getItem('login-email') && localStorage.getItem('login-pass') ? false : true,
            isRegister: false,
            onLogin: false,
            newMessage: {}
        }
    }

    componentDidMount() {
        this.handleLogin()
        $(window).resize(() => {
            this.handleContentHeight()
        }).ready(() => {
            this.handleContentHeight()
        })
    }

    initial() {
        let t = this
        qiscus.init({
            AppId: 'reactjs-mnhutthtajscb',
            options: {
                commentDeletedCallback: function (data) {},
                commentDeliveredCallback: function (data) {
                    $('.messages .message .chat .status[src="./sent.svg"]').attr({'src':'./delivered.svg', 'title':'Delivered'})
                },
                commentReadCallback: function (data) {
                    let prop = {'src':'./read.svg', 'title':'Read'}
                    $('.messages .message .chat .status[src="./delivered.svg"]').attr(prop)
                    setTimeout(function(){ $('.messages .message .chat .status[src="./sent.svg"]').attr(prop)},50)
                },
                presenceCallback: function (data) {
                    // onlineStatus()
                },
                typingCallback: function (data) {
                    // if (data.message == '1') {
                    //     vm.typingRoom = data.room_id
                    // } else {
                    //     vm.typingRoom = null
                    // }
                },
                onReconnectCallback: function (data) {},
                newMessagesCallback: function (messages) { t.setState({newMessage: messages[0]}) },
                roomClearedCallback: function (data) {}
            }
        })
    }
    
    switchLogin() {
        if (!this.state.onLogin) {
            this.setState({isRegister: !this.state.isRegister})
            $('.login-card input').val('').removeClass('is-valid is-invalid')
            $('#loginEmail').focus()
        }
    }

    doLogin() {
        let err = 0
        $('.login .card input').removeClass('is-valid is-invalid').each(function(){
            if ($(this).val() === '') {
                $(this).addClass('is-invalid')
                err = 1
            } else if ($(this).attr('type') === 'email') {
                var isEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/igm
                if (!isEmail.test($(this).val())) {
                    $(this).addClass('is-invalid')
                    err = 1
                }
            }
        })

        if (this.state.isRegister) {
            if ($('#loginPass').val() !== $('#loginCPass').val()) {
                $('#loginCPass').addClass('is-invalid')
                err = 1
            }
        }

        if (err) {
            $('.login .card input:not(.is-invalid)').each(function(){
                $(this).addClass('is-valid')
            })

            $('.login .card input.is-invalid:first').focus()
        } else {
            this.setState({onLogin: true})
            let email = $('#loginEmail').val()
            let pass  = $('#loginPass').val()
            let name  = $('#loginName').length ? $('#loginName').val() : ''
            qiscus.setUser(email, pass, name, '', {}).then((authData) => {
                this.setState({onLogin: false})
                if (qiscus.isLogin) {
                    localStorage.setItem('login-email', email)
                    localStorage.setItem('login-pass', pass)
                    $('root').html('')
                    this.setState({login: false})
                } else {
                    alert('Wrong password!')
                    $('#loginPass').focus()
                }
            }).catch((error) => alert(error))
        }
    }

    renderLoginPage = () => {
        return (
            <div className='login body'>
                <div className='card'>
                    <h4>{this.state.isRegister ? 'Register' : 'Login'}</h4>
                    <input id="loginEmail" type="email" placeholder="Enter your email" ref={(input) => {this.emailInput = input}}/>
                    <input id="loginPass" type="password" placeholder="Enter your password"/>
                    {this.state.isRegister && 
                        <div>
                            <input id="loginCPass" type="password" placeholder="Confirm password"/>
                            <input id="loginName" type="text" placeholder="Enter your name"/>
                        </div>
                    }
                    <button className="btn-main full" type="button" onClick={ () => this.doLogin() } disabled={this.state.onLogin}>
                        {this.state.onLogin ? <div className="loader sm"></div> : 'Submit'}
                    </button>
                    <button className="btn link text-main" onClick={ () => this.switchLogin()}>Click here to {this.state.isRegister ? 'login' : 'register'}</button>
                </div>
            </div>
        )
    }

    renderHomePage = () => {
        return (
            <HomePage
                qiscus={qiscus}
                email={localStorage.getItem('login-email')}
                pass={localStorage.getItem('login-pass')}
                newMessage={this.state.newMessage}
                logout={this.handleLogout}
            />
        )
    }

    handleLogin = () => {
        if (this.state.login) {
            setTimeout(() => {this.emailInput.focus()},1)
            $('body').on('keyup', (e) => {
                if (e.keyCode === 13) this.doLogin()
            })
        }
    }

    handleLogout = () => this.setState({login: true}, () => this.handleLogin())

    handleContentHeight = () => {
        let vh = window.innerHeight
        document.documentElement.style.setProperty('--vh', vh+'px')
    }

    render() {
        this.initial()
        if (this.state.login) {
            return (this.renderLoginPage())
        } else {
            return (this.renderHomePage())
        }
    }
}

ReactDOM.render(<Init />, document.getElementById('root'))
serviceWorker.register();