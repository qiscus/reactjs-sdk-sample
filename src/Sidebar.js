import React, { useState } from 'react';
import $ from 'jquery';
import { CSSTransition } from 'react-transition-group';
import { Modal } from 'react-bootstrap';
var qiscus

export default class Sidebar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.name,
            avatar: this.props.avatar,
            menu: false,
            updateNameModal: false
        }

        qiscus = this.props.qiscus
    }

    componentDidMount() {
        $(window).click((e) => {
            if (!$(e.target).hasClass('profile-menu')) {
                this.setState({menu: false})
            }
        })
    }

    toggleSidebar = () => { this.props.toggleSidebar(false) }
    handleOpen = () => $('#updateName').focus()
    handleClose = () => this.setState({ updateNameModal: false })
    handleChangeName = () => {
        qiscus.updateProfile({name: $('#updateName').val()}).then(() => {
            this.setState({ name: $('#updateName').val() })
            this.handleClose()
        }).catch(err => alert(err))
    }

    handleSearch() {
        let val = $('.search input').val().toLowerCase()
        for (let i = 1; i <= $('.rooms .content').length; i++) {
            let el = $('.rooms .content:nth-child('+i+')')
            el.find('h5').text().toLowerCase().indexOf(val) > -1 ? el.show() : el.hide()
        }
    }

    handleRoomClick = (id) => this.props.onRoomClick(id)
    getRoom = (id) => this.props.createRoom(id)

    handleLogout = () => {
        localStorage.clear()
        qiscus.disconnect()
        this.props.logout()
    }

    render() {
        let sidebar = 'sidebar'
        let roomClass = 'rooms'
        if (this.props.sidebar) sidebar += ' active'
        if (this.props.roomLoad) roomClass += ' item-flex-middle'

        return (
            <div className={sidebar}>
				<div className="header">
					<svg className="close-sidebar" onClick={this.toggleSidebar} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"/></svg>
					<img src={this.state.avatar} alt="Your avatar belongs here" />
					<h2 className="ellipsis font-weight-bold" title={this.state.name}>{this.state.name}</h2>
                    <div className="menu">
                        <svg className="menu profile-menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" onClick={() => this.setState({menu: !this.state.menu})}>
                            <path className="profile-menu" d="M96 184c39.8 0 72 32.2 72 72s-32.2 72-72 72-72-32.2-72-72 32.2-72 72-72zM24 80c0 39.8 32.2 72 72 72s72-32.2 72-72S135.8 8 96 8 24 40.2 24 80zm0 352c0 39.8 32.2 72 72 72s72-32.2 72-72-32.2-72-72-72-72 32.2-72 72z"/>
                        </svg>
                        <CSSTransition
                            in={this.state.menu}
                            timeout={100}
                            classNames="fade-menu"
                            unmountOnExit
                        >
                            <div>
                                <button onClick={() => this.setState({ updateNameModal: true })}>Change display name</button>
                                <button onClick={this.handleLogout}>Logout</button>
                            </div>
                        </CSSTransition>
                    </div>
				</div>
                <div className="search">
                    <input type="text" placeholder="Search room" onInput={() => this.handleSearch()} />
                </div>
                <div className={roomClass}>
                    <RoomList isLoad={this.props.roomLoad} rooms={this.props.roomList} onRoomClick={this.handleRoomClick} activeRoom={this.props.activeRoom} />
                </div>
				<CreateRoom getRoomId={ this.getRoom }/>

                <Modal size="sm" show={this.state.updateNameModal} onEntered={this.handleOpen} centered onHide={this.handleClose}>
                <Modal.Body>
                    <input type="text" id="updateName" className="form-control mb-3" defaultValue={this.state.name} placeholder="Enter your display name" />
                    <div className="row">
                        <div className="col-6 pr-2">
                            <button className="btn btn-secondary btn-sm btn-block" onClick={this.handleClose}>Cancel</button>
                        </div>
                        <div className="col-6 pl-2">
                            <button className="btn-main btn-sm btn-block" onClick={this.handleChangeName}>Change</button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
            </div>
        )
    }
}

function CreateRoom(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const onRoomClick = (id) => {
        props.getRoomId(id)
        setShow(false)
    }

    const handleSearch = () => {
        let val = $('.modal-body input').val().toLowerCase()
        for (let i = 1; i <= $('.rooms-modal .content').length; i++) {
            let el = $('.rooms-modal .content:nth-child('+i+')')
            el.find('h5').text().toLowerCase().indexOf(val) > -1 ? el.show() : el.hide()
        }
    }

    return (
        <>
            <div className="footer">
                <button className="btn-main full" onClick={handleShow}>Create Room</button>
            </div>
            <Modal size="sm" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                <Modal.Title id="room-modal-title">Open/Create New Room</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <input type="text" className="mb-3" placeholder="Search user" onInput={handleSearch} />
                    <RoomModalList hideModal={onRoomClick}/>
                </Modal.Body>
            </Modal>
        </>
    )
}

class RoomModalList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoad: true,
            roomList: []
        }
    }

    componentDidMount() {
        qiscus.getUsers('', 1, 9999999).then((data) => {
            let res = []
            for (let i = 0; i < data.users.length; i++) {
                let d = data.users[i]
                res.push(
                    <div key={d.id} className="content" onClick={() => this.handleClick(d.email)}>
                        <img src={d.avatar_url} className="rounded-circle" alt="" style={{width:'2rem',height:'2rem'}} />
                        <div className="info">
                            <h5 className="mb-0">{d.name}</h5>
                        </div>
                    </div>
                )
            }
            this.setState({
                isLoad: false,
                roomList: res
            })
        }).catch((error) => alert(error))
    }

    handleClick = (id) => this.props.hideModal(id)

    render() {
        let listClass = 'rooms-modal'
        let listStyle = {}
        let loader
        if (this.state.isLoad) {
            listClass += ' item-flex-middle'
            listStyle = {height: '4rem'}
            loader = <div className="loader md"></div>
        }
        return (
            <div className={listClass} style={listStyle}>
                {loader}
                {this.state.roomList}
            </div>
        )
    }
}

class RoomList extends React.Component {
    handleClick = (id) => this.props.onRoomClick(id)

    render() {
        if (this.props.isLoad) {
            return (<div className="loader lg"></div>)
        } else {
            if (this.props.rooms.length > 0) {
                let rooms = this.props.rooms
                let res = []
                
                for (let i = 0; i < rooms.length; i++) {
                    let roomClass = 'content'
                    let title = rooms[i].last_comment_message !== '' ? rooms[i].last_comment_message : ''
                    if (rooms[i].id === this.props.activeRoom) roomClass += ' active'
                    res.push(
                        <div className={roomClass} key={rooms[i].id} onClick={() => this.handleClick(rooms[i].id)}>
                            <img src={rooms[i].avatar} alt="" />
                            <div className="info">
                                <h5 className="m-0 ellipsis">{rooms[i].name}</h5>
                                <small className="d-block text-truncate" title={title}>
                                    {rooms[i].last_comment_message !== '' ? rooms[i].last_comment_message : <i>Empy chat</i>}
                                </small>
                            </div>
                        </div>
                    )
                }

                return res
            } else {
                return (<label className='no-room'>You don't have any room yet.</label>)
            }
        }
    }
}