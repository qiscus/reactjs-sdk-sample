import React from 'react';
import $ from 'jquery';
import { CSSTransition } from 'react-transition-group';
import Sidebar from './Sidebar';
import RoomContent from './RoomContent';
var qiscus

export default class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLogin: false,
            sidebar: true,
            displayName: '',
            displayImg: '',
            activeRoom: '',
            roomLoad: true,
            roomList: [],
            room: '',
            targetName: '',
            targetAvatar: '',
            targetStatus: '',
            isTyping: false,
            messagesLoad: false,
            messages: [],
            sendingMsg: false,
            sendingFile: false,
            hasLoadMore: false,
            onLoadMore: false,
            onDeleteMsg: false,
            deleteMsgModal: false
        }
        qiscus = this.props.qiscus
    }

    componentDidMount() {
        qiscus.setUser(this.props.email, this.props.pass, '', '', {}).then((authData) => {
            let d = qiscus.userData
            this.setState({
                isLogin: true,
                name: d.username,
                avatar: d.avatar_url
            })
            qiscus.loadRoomList({page:1, limit:100, show_participants:2, show_empty:false}).then((rooms) => {
                this.setState({roomLoad: false, roomList: rooms})
            }).catch((error) => alert(error))
        }).catch((error) => alert(error))

        $(window).resize(() => {
            if (!$('.open-sidebar').is(':visible')) this.setState({sidebar: true})
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.newMessage !== prevProps.newMessage) {
            let msg = this.props.newMessage
            let rooms = this.state.roomList
            let i = rooms.findIndex(room => room.id === msg.room_id)
            if (rooms[i]) rooms.splice(i,1)
            rooms.unshift({
                id: msg.room_id,
                avatar: msg.room_avatar,
                name: msg.room_name,
                last_comment_message: msg.message
            })
            this.setState({roomList: rooms})

            if (this.state.room.id === msg.room_id) {
                let data = this.state.messages.concat({
                    id: msg.id,
                    pos: msg.email === localStorage.getItem('login-email') ? 'right' : 'left',
                    msg: msg.message,
                    date: this.getMsgTime(msg.timestamp),
                    status: msg.status,
                    unique_id: msg.unique_temp_id,
                    payload: msg.payload
                })
                this.setState({messages: data})

                if (msg.email === localStorage.getItem('login-email')) {
                    this.setState({ sendingMsg: false})
                    if (msg.payload.content) this.setState({sendingFile: false})
                    setTimeout(function(){
                        $('.send-msg-input').val('').focus()
                        $('.messages').prop({ scrollTop: $('.messages').prop("scrollHeight") })
                    },50)
                }
            }
        }
    }

    toggleSidebar = (state) => this.setState({sidebar: state})

    handleRoomClick = (id) => {
        this.setState({activeRoom: id})
        if ($('.open-sidebar').is(':visible')) this.setState({sidebar: false})
        this.getRoomChat(id)
    }

    handleCreateRoom = (id) => {
        qiscus.chatTarget(id).then((room) => {
            this.setState({activeRoom: room.id})
            if ($('.open-sidebar').is(':visible')) this.setState({sidebar: false})

            let i = this.state.roomList.findIndex(r => r.id === room.id)
            if (i === -1) {
                let rooms = this.state.roomList
                rooms.unshift(room)
                this.setState({roomList: rooms})
            }

            this.getRoomChat(room.id)
        }).catch((error) => alert(error))
    }

    getRoomChat = (id) => {
        this.setState({
            targetName: '',
            targetAvatar: '',
            targetStatus: '',
            messagesLoad: true,
            messages: [],
            hasLoadMore: false
        })

        qiscus.getRoomById(id).then((room) => {
            this.setState({
                targetName: room.name,
                targetAvatar: room.avatar,
                targetStatus: 'Offline'
            })

            let msg = room.comments
            let data = []
            console.log(msg)
            for (let i = 0; i < msg.length; i++) {
                let d = msg[i]
                data.push({
                    id: d.id,
                    pos: d.username_real === localStorage.getItem('login-email') ? 'right' : 'left',
                    msg: d.message,
                    date: this.getMsgTime(d.timestamp),
                    status: d.status,
                    unique_id: d.unique_id,
                    payload: d.payload
                })
            }

            let loadMore = msg.length === 20 ? true : false
            this.setState({messagesLoad: false, messages: data, room: room, hasLoadMore: loadMore})
            $('.messages').prop({ scrollTop: $('.messages').prop("scrollHeight") })
            $('.send-msg-input').focus()
        }).catch((error) => alert(error))
    }

    getMsgTime(time) {
        let d = new Date(time)
        return {
            time: this.zeroFirst(d.getHours()) + ":" + this.zeroFirst(d.getMinutes()),
            date: d.getDate()+'/'+d.getMonth()+'/'+d.getFullYear()
        }
    }

    zeroFirst(t) {
        if (t < 10) t = "0" + t
        return t
    }

    onSendMsg = () => this.setState({sendingMsg: true})
    onSendFile = () => this.setState({sendingFile: true})

    loadMore = () => {
        this.setState({onLoadMore: true})
        var lastId = this.state.messages[0].id
        var lastElPos = $('.messages .message:first').position().top
        var options = {last_comment_id: lastId, limit: 20}
        qiscus.loadComments(this.state.room.id, options).then((msg) => {
            let pastMessages = []
            for (let i = 0; i < msg.length; i++) {
                let d = msg[i]
                pastMessages.push({
                    id: d.id,
                    pos: d.email === localStorage.getItem('login-email') ? 'right' : 'left',
                    msg: d.message,
                    date: this.getMsgTime(d.timestamp),
                    status: d.status,
                    unique_id: d.unique_temp_id,
                    payload: d.payload
                })
            }

            let loadMore = msg.length === 20 ? true : false
            let data = pastMessages.concat(this.state.messages)
            this.setState({messages: data, onLoadMore: false, hasLoadMore: loadMore})
            
            setTimeout(() => {
                let i = this.state.messages.findIndex(chat => chat.id === lastId) + 1
                var curPos = $('.messages .message:nth-child('+i+')').position().top - lastElPos
                $('.messages').scrollTop(curPos)
            },10)
        }).catch((error) => alert(error))
    }

    handleDeleteMsg = (id) => {
        this.setState({onDeleteMsg: true})
        qiscus.deleteComment(this.state.room.id, [id]).then((comment) => {
            let chats = this.state.messages
            let i = chats.findIndex(chat => chat.unique_id === id)
            let isLastMsg = i === chats.length -1 ? true : false
            chats.splice(i,1)
            let rooms = this.state.roomList

            if (isLastMsg) {
                rooms[rooms.findIndex(room => room.id === this.state.room.id)].last_comment_message = chats.length > 0 ? chats[chats.length -1].msg : ''
            }
            
            this.setState({
                messages: chats,
                roomList: rooms,
                onDeleteMsg: false,
                deleteMsgModal: false
            })
        }).catch((error) => alert(error))
    }

    toggleDeleteMsgModal = (state) => this.setState({deleteMsgModal: state})

    handleDeleteRoom = () => {
        let rooms = this.state.roomList
        let i = rooms.findIndex(room => room.id === this.state.room.id)
        if (rooms[i]) rooms.splice(i,1)
        this.setState({
            activeRoom: '',
            room: '',
            targetName: '',
            targetAvatar: '',
            targetStatus: '',
            messages: []
        })
    }

    handleLogout = () => this.props.logout()

    render() {
        if (this.state.isLogin) {
            return (
                <div style={{display:'flex'}}>
                    <CSSTransition
                        in={this.state.sidebar}
                        timeout={300}
                        classNames="fade"
                        unmountOnExit
                    >
                        <Sidebar
                            sidebar={this.state.sidebar}
                            toggleSidebar={this.toggleSidebar}
                            name={this.state.name}
                            avatar={this.state.avatar}
                            qiscus={qiscus}
                            createRoom={this.handleCreateRoom}
                            roomLoad={this.state.roomLoad}
                            roomList={this.state.roomList}
                            onRoomClick={this.handleRoomClick}
                            activeRoom={this.state.activeRoom}
                            logout={this.handleLogout}
                        />
                    </CSSTransition>
                    <RoomContent
                        qiscus={qiscus}
                        sidebar={this.state.sidebar}
                        toggleSidebar={this.toggleSidebar}
                        isLoad={this.state.messagesLoad}
                        name={this.state.targetName}
                        avatar={this.state.targetAvatar}
                        status={this.state.targetStatus}
                        isTyping={this.state.isTyping}
                        messages={this.state.messages}
                        room={this.state.room}
                        sendingMsg={this.state.sendingMsg}
                        onSendMsg={this.onSendMsg}
                        sendingFile={this.state.sendingFile}
                        onSendFile={this.onSendFile}
                        loadMore={this.loadMore}
                        hasLoadMore={this.state.hasLoadMore}
                        onLoadMore={this.state.onLoadMore}
                        deleteMsg={this.handleDeleteMsg}
                        deleteMsgModal={this.state.deleteMsgModal}
                        toggleDeleteMsgModal={this.toggleDeleteMsgModal}
                        deleteRoom={this.handleDeleteRoom}
                    />
                </div>
            )
        } else {
            return (
                <div className="init-loading">
                    <div className="loader lg"></div>
                </div>
            )
        }
    }
}